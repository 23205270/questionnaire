<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quest_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('questions_id')->unsigned();
            $table->string('result_detail');

            $table->foreign('questions_id')->references('id')->on('questions')->onDelete('cascade');

            // $table->primary(['questions_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quest_results');
    }
}
