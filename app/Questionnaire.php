<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionnaire extends Model
{
    public function Question()
    {
      return $this->hasOne('App\Question');
    }

    public function User()
    {
      return $this->belongsTo('App\User');
    }

    protected $fillable = [
        'name', 'users_id', 'date_created',
    ];
}
