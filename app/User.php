<?php

    namespace App;

    use Illuminate\Foundation\Auth\User as Authenticatable;

    class User extends Authenticatable
    {
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'name', 'email', 'password',
        ];

        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = [
            'password', 'remember_token',
        ];

        /*
         *  relation to the role model
         */

        public function Questionnaire()
        {
          return $this->hasOne('App\Questionnaire');
        }

        public function Role_user()
        {
          return $this->hasOne('App\Role_user');
        }
}
