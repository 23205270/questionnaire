<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quest_answers extends Model
{
    public function quest_num()
    {
      return $this->belongsTo('App\Quest_num');
    }
}
