<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Roles extends Model
{
    public function Role_user()
    {
      return $this->hasOne('App\Role_user');
    }

    protected $fillable = [
        'name', 'label', 'updated_at', 'created_at',
    ];
}
