<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quest_num extends Model
{
    public function quest_answers()
    {
      return $this->hasOne('App\Quest_answers');
    }

    public function Question()
    {
      return $this->hasOne('App\Question');
    }
}
