<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role_user extends Model
{
    public function User()
    {
      return $this->belongsTo('App\User');
    }

    public function Roles()
    {
      return $this->belongsTo('App\Roles');
    }
}
