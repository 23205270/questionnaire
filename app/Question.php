<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
    public function Quest_num()
    {
      return $this->belongsTo('App\Quest_num');
    }

    public function Quest_results()
    {
      return $this->hasOne('App\Quest_results');
    }

    public function Questionnaire()
    {
      return $this->belongsTo('App\Questionnaire');
    }
}
