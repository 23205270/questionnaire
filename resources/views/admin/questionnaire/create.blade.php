@extends('layouts.app')
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Questionnaire</title>
    <!-- <link rel="stylesheet" type="text/css" href="/css/app.css"> -->


    @section('content')
</head>
<body>
<h1>Create Questionnaire</h1>

{!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire')) !!}
        {{ csrf_token() }}
    <div class="row large-12 columns">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Question', ['class' => 'button']) !!}
    </div>
{!! Form::close() !!}
@endsection
</body>
</html>
