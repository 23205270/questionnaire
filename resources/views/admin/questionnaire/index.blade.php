@extends('layouts.app')
<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>All Questionnaires</title>
    <!-- <link rel="stylesheet" type="text/css" href="/css/app.css"> -->
</head>
<body>
  @section('content')
   <section>
     @if (isset ($articles))

         <ul>
             @foreach ($questionnaires as $questionnaire)
                 <li>{{ $article->title }}</li>
             @endforeach
         </ul>
     @else
         <p> no questionnaires added yet </p>
     @endif
 </section>

 {!! Form::open(['']) !!}
     <div class="row">
         {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
     </div>
 {!! Form::close() !!}
 @endsection
 </body>
 </html>
