@extends('layouts.app')
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>All Users</title>
</head>
<body>
@section('content')
<h1>All Users</h1>

<section>
    @if (isset ($users))

        <table>
            <tr>
                <th>Username</th>
                <th>Email</th>
                <!-- <th>Role</th> -->

            </tr>
            @foreach ($users as $user)
                <tr>
                    <td><a href="/admin/users/{{ $user->id }}" name="{{ $user->name }}">{{ $user->name }}</a></td>
                    <td> {{ $user->email }}</td>
                </tr>
             @endforeach

      </table>
    @else
        <p>no users</p>
    @endif
</section>
{!! Form::open(['']) !!}
    <div class="row">
        {!! Form::submit('Add User', ['class' => 'button']) !!}
    </div>
{!! Form::close() !!}
@endsection
</body>
</html>
