<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>All Roles</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
<h1>Roles</h1>

<section>
    @if (isset ($roles))

        <table>
            <tr>
                <th>Name</th>
                <th>Label</th>
            </tr>
            @foreach ($roles as $role)
                <tr>
                    <td><a href="/admin/roles/{{ $role->id }}" name="{{ $role->title }}">{{ $role->title }}</a></td>
                    <td>
                        <ul>
                            @foreach($role->permissions as $permission)
                                <li>{{ $permission->label }}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
            @endforeach
        </table>
    @else
        <p>no roles</p>
    @endif
</section>
{{ Form::open(array('action' => 'RoleController@create', 'method' => 'get')) }}
    <div class="row">
        {!! Form::submit('Add Role', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}
</body>
</html>
