<!-- <!doctype html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Edit Role</title>
      <link rel="stylesheet" type="text/css" href="/css/app.css">
      </head>
  <body>
      <h1>Edit Role</h1>

      {!! Form::open(array('action' => 'RoleController@store', 'id' => 'editrole')) !!}
              {{ csrf_field() }}


  </body>
</html> -->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <h1>Edit - {{ $role->name }}</h1>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
  <nav>
      <ul>
          <li><a href="/admin/questionnaire/create">Create a new Questionnaire</a></li>
          <li><a href="/admin/questionnaire">View Questionnaires</a></li>
          <li><a href="/admin/users/create">Create a User</a></li>
          <li><a href="/admin/users">See all users</a></li>
          <li><a href="/admin/results">View all results</a></li>
      </ul>
  </nav>

  <h1>Edit - {{ $role->name }}</h1>
  {!! Form::model($user, ['method' => 'PATCH', 'url' => '/admin/users/' . $user->id]) !!}
  <div class="row large-12 columns">
      {!! Form::label('name', 'Name:') !!}
      {!! Form::text('name', null, ['class' => 'large-8 columns']) !!}
  </div>

  <div class="row large-12 columns">
      {!! Form::label('label', 'Label:') !!}
      {!! Form::text('label', null, ['class' => 'large-8 columns']) !!}
  </div>

  <div class="row large-4 columns">
      {!! Form::submit('Edit Role', ['class' => 'button']) !!}
  </div>
  {!! Form::close() !!}


  <div>
      {!! Form::submit('Update User and Roles') !!}
  </div>
  {!! Form::close() !!}

</body>
</html>
